# LumosStick

<img align="right" style="padding: 0 0 0 12px;" src="files/lumosstick_512.jpg"/>

The LumosStick is a programmable neopixel 'display' consisting of a 7x40 matrix of 280 LEDs.
It also has a tone buzzer and a pair of buttons to add interactions.

The LumosStick is a USB device running a CircuitPython program.

The LumosStick behavior and features are totally customizable using CircuitPython - an easy to use programming language. And if you really don't want to learn to program, that's OK too. The LumosStick is preprogrammed as a clock and there are other samples you can simply drag-and-drop to run.

The API provides easy-to-use configuration and operations capabilities.

If you are just starting out with the LumosStick, take a look at the [**README**](README.md) for an overview and some easy customization examples to get you started!

[[_TOC_]] 


## LumosStick API

The LumosStick library provides a set of _methods_ and _properties_ which you use in your `code.py` to customize the behavior of you LumosStick device. 

>>>
Methods and properties are terms used in CircuitPython programming. They are mentioned here for completeness but are not significant for customizing the LumosStick. The terms _method_ and _property_ are common to many programming languages. A _method_ is a capability of a library to perform an action. A _property_ is a piece of information of the library which may get looked at and some _properties_ may be changed. _(not all properties may be changed_)
>>>

What follows are the details of the library and what each function or attribute does as well as what inputs/outputs each has.

The LumosStick library provides access to its general operations, the buttons, LEDs, the buzzer, and has some convenient pre-defined keywords.

Here is a block diagram of the LumosStick library ...

<img align="center" style="width: 90%" src="files/block_diagram.png"/>

Before you can use the LumosStick library, you need to load it. Near the top of you `code.py` CircuitPython program, you will _import_ the library. The simplest method is with:

```python
from lumosstick import LumosStick   # REQUIRED: import the LumosStick library
```

### Operational Capabilities

#### LumosStick Constants

The LumosStick library begins with its operational capabilities. These include a number of pre-defined _properties_ to make programming easier and more readable. Each predefine property starts with `LumosStick.` and is then in all capital letters, as in `LumosStick.PRESSED` which is used to compare a key action to check if is pressed or released.

Here are all of the pre-defined properties:

| **Definition**    | **Description**                                      |
|:------------------|:--------------------------------------------------|
| `LumosStick.CLOCK`    | initialize the LumosStick as a clock |
| `LumosStick.TIMER`    | initialize the LumosStick as a timer |
| `LumosStick.COUNTDOWN`    | initialize the LumosStick as a countdown timer |
| `LumosStick.NOCLOCK`    | initialize the LumosStick with no timing features |
| `LumosStick.ONESECOND`  | value used for making delta time changes |
| `LumosStick.ONEMINUTE`  | value used for making delta time changes |
| `LumosStick.ONEHOUR`  | value used for making delta time changes |
| `LumosStick.LEFT`     | indicates a button event is from the left button |
| `LumosStick.RIGHT`    | indicates a button event is from the right button |
| `LumosStick.PRESSED`  | indicates the button has been pressed |
| `LumosStick.RELEASED` | indicates the button has been released |
| `LumosStick.RED`      | used for changing LED color |
| `LumosStick.GREEN`    | used for changing LED color |
| `LumosStick.ORANGE`   | used for changing LED color |
| `LumosStick.YELLOW`   | used for changing LED color |
| `LumosStick.BLUE`     | used for changing LED color |
| `LumosStick.MAGENTA`  | used for changing LED color |
| `LumosStick.PURPLE`   | used for changing LED color |
| `LumosStick.PINK`     | used for changing LED color |
| `LumosStick.CYAN`     | used for changing LED color |
| `LumosStick.WHITE`    | used for changing LED color |
| `LumosStick.GRAY`     | used for changing LED color |
| `LumosStick.DKGRAY`   | used for changing LED color |
| `LumosStick.BLACK`    | used for changing LED color _(black will turn an LED off)_ |


#### LumosStick Instance

Your `code.py` CircuitPython program needs to create an _instance_ of the LumosStick library. This provides access to all of the capabilities of the library. The simplest method is with:

```python
lumos = LumosStick(mode=LumosStick.CLOCK) # this is the default for creating your LumosStick object
```

#### LumosStick Settings

At anytime after you have created your LumosStick instance, you may set the default colors or brightness of the LEDs, change the operating mode, or change specific clock features. Here are examples of these settings:

```python
lumos.mode = LumosStick.TIMER
lumos.background = LumosStick.BLACK
lumos.color = 0xff7f00
lumos.brightness = 0.20
```

Colors may be specified using the predefined choices _(see **Constants** above)_ or using hexadecimal values where the red, green, and blue portions are values from 00 to FF. For example, a mix of 50% red and 25% blue would be `0x7F003F`.

The `background` and `color` are each a single value. These are the default colors when setting LEDs.

The `brightness` affects both the LEDs which make up the clock ring as well as the LEDs which form the block in the center. The value is between 0.01-0.75. The value is constrained to this range to insure the LumosStick does not draw excess power from the USB connection.


#### LumosStick WiFi Setup

The LumosStick `CLOCK` mode will use an internet service to automatically set the time. The use of internet time requires the `secrets.py` file on the `CIRCUITPY` storage device to contain the proper WiFi setup and account information for Adafruit's IO service. The `secrets.py` file has the following entries ...

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
    'aio_username' : 'your_aio_username',
    'aio_key' : 'your_aio_keystring',
    'timezone' : 'America/New_York', 
}
```

The `ssid` and `password` are the settings for your own WiFi. The two `aio_*` settings are for Adafruit's IO services. These services are very easy to use.
Take a look at the [Adafruit IO](https://io.adafruit.com) website. If you do not yet have an account and would like to use the service to get the time, then create an account and then create a key.


#### LumosStick Execution

Most of the LumosStick execution is handled by `update()`. This needs to be called frequently. Don't worry about calling it too often.

The update is performed within your `code.py` `while True:` loop and looks like this ...

```python
    lumos.update() # this is very important so everything has a chance to check for new information
```

The `update()` performs all the necessary periodic tasks including getting new information about the joystick and its push-button, updating the screen, and updating the LEDs.


### LumosStick Clock and Timers

The LumosStick library simplifies building chrono-related projects like clocks, timers, and timing based games.

#### LumosStick Time

There are properties to get and set the current time using a `datetime` or using individual properties for hour, minute, and second. Here are several examples ...

```python
    current_time = lumos.time         # get the current time as a datetime 
    lumos.time  = current_time        # set the current time using a datetime 
    lumos.time += LumosStick.ONEHOUR  # increment time using one of the constants
    current_hour = lumos.hour         # get the current hour 00..23
    current_minute = lumos.minute     # get the current minute 00..59
    current_second = lumos.second     # get the current second 00..59
    lumos.hour += 1                   # set the current hour one hour forward
    lumos.time = (12,30,0)            # set the time to 12:30 and zero seconds
```

#### LumosStick Timer

The LumosStick library provides support for `CLOCK` and also support for both `TIMER` and `COUNTDOWN`. These timer modes have two methods and one property ...

```python
    lumos.stop()
    lumos.start()

    if lumos.stopped:
      # do something
```

The `stop()` and `start()` methods control the running of the timer. The `stopped` property is either `True` or `False` for the timer actively running. For `COUNTDOWN` mode, the timer automatically stops when the time runs down to 00:00:00. Use the `stopped` property to determine if the time has run out.


### Buttons

The buttons are processed automatically and is independent of the `update()` operation.

The property `buttons_changed` is `True` if either button have been pressed or released. Each time `buttons_changed` is checked, the corresponding data available from the  `button` property. 

The `button` returns two values ...

```python
    button, action = lumos.button # get button data
```
The `button` value with be either `LumosStick.LEFT` or `LumosStick.RIGHT` and the `action` will be either `LumosStick.PRESSED` or `LumosStick.RELEASED`.

To process all button changes, you use a `while` loop ...

```python
    while lumos.buttons_changed:
        # lumos.button has the new key data
        button, action = lumos.button # get button data
        ...
        # do something with the button and action
```

### LEDs

There LumosStick matrix is made up of addressable RGB LEDs called neopixels. _(Adafruit provides [this introduction to](https://learn.adafruit.com/circuitpython-essentials/circuitpython-neopixel) neopixels.)_ 

The matrix is made up of 280 neopixels organized as 7 rows of 40 LEDs each.

With the LumosStick in landscape orientation with the edge buttons on the left. The LEDs are numbered left to right and top to bottom. The `0` row of LEDs is at the top. The `0` column is at the left.

**Note:** The LumosStick library is not required to use the LumosStick board. The library aids with chrono-related projects and text projects. For more artistic control and completely custom applications, the LumosStick board may be programmed with Adafruit and third-party libraries. It is also possible to program the LumosStick board with Arduino or Espressif. The [bounce demo](https://gitlab.com/bradanlane/bounce) is written in C language using the FastLED library using VSCode_PlatformIO.


The default LED color is set using the `color` property _(see `color` in 'LumosStick Settings' above)_

An LED color is changed using either `led(x,y)` to set an LED to the current default color, or `led(x, y, color)` to set an LED to a specific color without affecting the defaults. Color may be one of the predefined colors _(see `LumosStick Constants` above)_ or a custom color. A custom color is created using HEX notation e.g. `0xA20025` is crimson.

All of the LEDs may be changed at once using `leds()` to set all LEDs to the current default color, or `leds(color)` to set all LEDs to a specific color without affecting the default setting.

The brightness of the LEDs is controlled by the `brightness` property.

```python
    lumos.leds(LumosStick.BLUE)                                 # set all LEDs in the ring to blue
    lumos.led(29, 3)                                            # assuming portrate orientation, set the 30th LED in the 4th row to the current default color
    lumos.digits(14, number)                                    # render a 1 or 2 digit number using the current default color
    lumos.letters(10, text, LumosStick.GREEN, LumosStick.GRAY)  # render up to 2 characters in the block
```

**Note:** Remember the numbering starts as `0`.

Colors may be provided using the predefined colors listed in the **Constants** section above or as a hexadecimal number. The methods for adding content to the block of LEDs in the center of the LumosStick board have a default background color of `BLACK`. You may omit providing a background color if you want `BLACK`.

The digits and letters are rendered using the `5x7.pcf` font from the `font/` folder on the `CIRCUITPY` storage device.


### Buzzer

The LumosStick has a very simple buzzer. It is able to produce a simple tone. It will nto play sound files.

The buzzer may play a tone for a period of time or it may produce a tone until instructed to stop.

The tone is given as a frequency from 0 to 16K. In real terms, a value below 8K is recommended.

Use `tone(frequency, duration)` to play a tone for a fixed amount of time. The duration is in seconds and may be a decimal number, e.g. `0.5` is one half second.

**Caution:** The `tone()` will only finish after `duration`. Do not use a long duration if you are updating the LEDs frequently because these tasks will wait until `tone()` completes.

An alternative to `tone()` is to use `toneon(frequency)` and `toneoff()`.

Here is an example of playing a tone while the right button is pressed and stopping the tone when the right button is released ...

```python
while True:
    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosStick.RIGHT):
          if action:
                lumos.toneon(440) # 'A' on the music scale
          else:
              lumos.toneoff()
```


### SimpleTimer

Many LumosStick projects want to perform tasks or make changes periodically. 
Sometimes these need to occur based on time but often they just need to happen at a given interval. 
For intervals, CircuitPython has a millisecond counter using `supervisor.ticks_ms()`. 
However, this wraps from its maximum value back to zero.

The SimpleTimer class is provided to make it easy to create a timer and check when it has expired. 
A SimpleTimer instance is created and assigned an interval. 
The timer is started and then checked to know when it has expired. 
It may be started again to repeat the process. 
If a new interval is needed, change the `interval` value before calling `timer.start()` again.

```python
timer = SimpleTimer(250)    # 250 milliseconds is 0.25 seconds
timer.start()

while True:
    if timer.expired:
        timer.start()   # start the timer again
        # do some task each time the timer expires
```



### Direct Access

The LumosStick hardware may be used without the LumosStick library. Here are the CircuitPython board pins on the WEMOS ESP32-S2 MiniS2 and their assignments.

| Board Pin       | Usage                                      |
|:-----------------|--------------------------------------------------|
| **board.IO11**   | Left button (open HIGH) |
| **board.IO12**   | Right button (open HIGH) |
| **board.IO33**   | Neopixel row 0 (has 40 LEDs)) |
| **board.IO34**   | Neopixel row 1 (has 40 LEDs)) |
| **board.IO35**   | Neopixel row 2 (has 40 LEDs)) |
| **board.IO36**   | Neopixel row 3 (has 40 LEDs)) |
| **board.IO37**   | Neopixel row 4 (has 40 LEDs)) |
| **board.IO38**   | Neopixel row 5 (has 40 LEDs)) |
| **board.IO39**   | Neopixel row 6 (has 40 LEDs)) |
| **board.IO07**   | Buzzer |


**Tip:** Check out the [examples](examples/) for a examples of many of the API capabilities.
The `scrolling` example is a good example of using using offset for letters or digits.
The `random` example is a good example of rendering text.
The `bounce` example is written in C language for the Arduino IDE or VSCode + PlatformIO and uses the popular FastLED library.

To load and example onto the LumosStick, copy the file to `CIRCUITPY` with the destination name `code.py`

