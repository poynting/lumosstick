"""
    fill in the fields below and then rename this file to secrets.py
    copy the secrets.py file to the CIRCUITPY device
    
    if you do not have an Adafruit IO key, follow this tutorial:
        https://learn.adafruit.com/adafruit-magtag/getting-the-date-time
"""

secrets = {
    'ssid' : 'ssid',
    'password' : 'password',
    'aio_username' : "name",
    'aio_key' : "keystring",
    'timezone' : "America/New_York", # http://worldtimeapi.org/timezones
}