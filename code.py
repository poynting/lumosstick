# code.py (LumosStick Clock example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosStick LEDs, buttons, and tone buzzer.

The LumosStick hardware connects via USB and provides:
    - 280 Neopixel LEDs (when held horizontal it is 7 rows of 40 LEDs each)
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosStick is powered by a Lolin ESP32-S2 which include Wifi connectivity.

The sample code used the Adafruit IO API to synchronize a digital-style LED clock.

Documentation: 
**Hardware:**
    * the LumosStick (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor
import random
import time

try:
    from LumosStick import LumosStick   # REQUIRED: import the LumosStick library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/lumosstick.py'")
    print ("The latest LumosStick library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/lumosstick")
    print("------------------------------------------------------------------------")


lumos = LumosStick(mode=LumosStick.CLOCK)
lumos.brightness = 0.15

while True:
    lumos.update()

    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosStick.RIGHT) and (action == LumosStick.PRESSED):
            lumos.brightness += 0.05
            if lumos.brightness > 0.3:
                lumos.brightness = 0.3
        if (button == LumosStick.LEFT) and (action == LumosStick.PRESSED):
            lumos.brightness -= 0.05
            if lumos.brightness < 0:
                lumos.brightness = 0
# end of while True loop
