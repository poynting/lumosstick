# code.py (LumosStick Scrolling Text example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosStick LEDs, buttons, and tone buzzer.

The LumosStick hardware connects via USB and provides:
    - 280 Neopixel LEDs (when held horizontal it is 7 rows of 40 LEDs each)
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosStick is powered by a Lolin ESP32-S2 which include Wifi connectivity.

The sample code scrolls a static text string.

Documentation: 
**Hardware:**
    * the LumosStick (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor
import random
import time

try:
    from LumosStick import LumosStick, SimpleTimer   # REQUIRED: import the LumosStick library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/lumosstick.py'")
    print ("The latest LumosStick library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/lumosstick")
    print("------------------------------------------------------------------------")



lumos = LumosStick(mode=LumosStick.NOCLOCK)
brightness = 0.15
lumos.brightness = brightness
lumos.color=LumosStick.GREEN

text = "The LumosStick by Bradan Lane STUDIO"
offset = 40

WAIT = 40   # milliseconds; this is about as fast as it can go
timer = SimpleTimer(WAIT)
timer.start()

# IMPORTANT: the text will look a bit jerky until all of the characters from teh font have been loaded

while True:
    lumos.update()

    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosStick.RIGHT) and (action == LumosStick.PRESSED):
            print("RIGHT")
            brightness += 0.05
            if brightness > 0.3:
                brightness = 0.3
            lumos.brightness = brightness
        if (button == LumosStick.LEFT) and (action == LumosStick.PRESSED):
            print("LEFT")
            brightness -= 0.05
            if brightness < 0:
                brightness = 0
            lumos.brightness = brightness

    if timer.expired:
        timer.start()
        offset -= 1 # shift to the left
        lumos.letters(offset, text)

        # if the last pixels of the text have scrolls off the left, then we reset and start over again
        if ((len(text) * 6) + offset) < 0:
            offset = 40

# end of while True loop
